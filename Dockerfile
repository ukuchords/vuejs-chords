# docker build -t my-perl-app .
# docker run -it -v "$(pwd)/public/pdf:/target" -it --rm --name my-running-app my-perl-app
# docker run -it -v "$(pwd)/public/pdf:/target" -it --rm --name my-running-app my-perl-app chordpro /songs/Ho_Hey.cho -o /target/HoHey.pdf

FROM perl:5.34
COPY /public/songs /songs
WORKDIR /usr/src/myapp
RUN cpan install chordpro
WORKDIR /usr/src/app
CMD [ "chordpro", "/songs/Bubbly.cho", "-o", "/target/Bubbly.pdf"]

