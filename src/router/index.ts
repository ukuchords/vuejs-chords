import { createRouter, createWebHistory } from 'vue-router'
import Home from '../pages/Home.vue'
import About from "../pages/About.vue";
import SongEditor from "../pages/SongEditor.vue";
import Mp3Player from "../pages/Mp3Player.vue";
import RhythmMachine from "../pages/RhythmMachine.vue";
import SingAlong from "../pages/SingAlong.vue";

const routes = [
    {
        path: '/', name: 'Home', component: Home
    },
    {
        path: '/about', name: 'About', component: About
    },
    {
        path: '/editor', name: 'SongEditor', component: SongEditor
    },
    {
        path: '/player', name: 'Mp3Player', component: Mp3Player
    },
    {
        path: '/rhythm', name: 'RhythmMachine', component: RhythmMachine
    },
    {
        path: '/singalong', name: 'SingAlong', component: SingAlong
    }
]

export default createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})
