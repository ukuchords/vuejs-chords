export class AudioAnimationCircle {

    private stop = false;

    public startAnimation(canvasContext: CanvasRenderingContext2D, analyser: AnalyserNode, gainNode: GainNode) {
        console.log("start animateCircle")
        this.stop = false;
        this.animateCircle(canvasContext, analyser,gainNode)
    }

    public stopAnimation() {
        console.log("stop animateCircle")
       this.stop = true;
    }

    private animateCircle( canvasContext: CanvasRenderingContext2D, analyser: AnalyserNode, gainNode: GainNode) {

        if (!this.stop) {
            window.requestAnimationFrame(() => this.animateCircle(canvasContext, analyser, gainNode))
        }
        const PI_TWO = Math.PI * 2;

        this.clearCanvas(canvasContext)
        if (canvasContext != null) {

            const frequencyDataLength = analyser.frequencyBinCount;
            const frequencyData: Uint8Array = new Uint8Array(frequencyDataLength);
            analyser.getByteFrequencyData(frequencyData);

            const cx = canvasContext.canvas.width / 2;
            const cy = canvasContext.canvas.height / 2;
            const radius = (Math.abs(canvasContext.canvas.width) / 10);

            canvasContext.beginPath();
            canvasContext.lineWidth = 1;
            let values: number[] = Array.from(frequencyData)
            const avg = this.getAvg(values) * gainNode.gain.value;
            canvasContext.arc(cx, cy, (avg + radius), 0, PI_TWO, false);

            canvasContext.stroke();
            canvasContext.fill();
            canvasContext.closePath();
        }
    }

    public clearCanvas( canvasContext: CanvasRenderingContext2D) {

        const background_gradient_color_1 = "#000011";
        const background_gradient_color_2 = "#060D1F";
        const background_gradient_color_3 = "#02243F";

        if (canvasContext != null) {
            let gradient: CanvasGradient | null = canvasContext.createLinearGradient(0, 0, 0, canvasContext.canvas.height);

            gradient.addColorStop(0, background_gradient_color_1);
            gradient.addColorStop(0.96, background_gradient_color_2);
            gradient.addColorStop(1, background_gradient_color_3);

            canvasContext.beginPath();
            canvasContext.globalCompositeOperation = "source-over";
            canvasContext.fillStyle = "#F451BA";
            canvasContext.fillRect(0, 0, canvasContext.canvas.width, canvasContext.canvas.height);
            canvasContext.fill();
            canvasContext.closePath();

            gradient = null;

        }
    }

    private getAvg(values: number[]) {
        let value = 0;

        values.forEach(function(v) {
            value += v;
        })

        return value / values.length;
    }
}