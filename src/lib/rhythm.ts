import * as Tone from "tone";
import {ToneAudioBuffers, ToneEventCallback} from "tone";

export interface SampleDefinition {

    id: string;
    name: string;
    file: string;
}

export const beatSampleDefinitions: SampleDefinition[] = [{"id": 'B1', name: 'Cowbell', file: 'cowbell.wav'}
    , {"id": 'B1', name: 'Cowbell closed', file: 'cowbell-large-closed.wav'}
    , {"id": 'B2', name: 'Cymbal', file: 'cymbal-hihat-foot-1.wav'}
    , {"id": 'B3', name: 'Drum', file: 'drum4afrofeet.wav'}
    , {"id": 'B4', name: 'Drum bass', file: 'drum-bass-lo-1.wav'}
    , {"id": 'B5', name: 'Hat', file: 'Hat_02.wav'}
    , {"id": 'B6', name: 'Kick', file: 'Kick_11.wav'}
    , {"id": 'B7', name: 'Rim', file: 'Rim_05.wav'}
    , {"id": 'B8', name: 'Shaker Hi', file: 'shaker-hi-up.wav'}
    , {"id": 'B9', name: 'Shaker Lo', file: 'shaker-lo-down.wav'}
    , {"id": 'B10', name: 'Snare', file: 'Snare_01.wav'}
    , {"id": 'B11', name: 'Timbale', file: 'timbale-hi-crosstick-2.wav'}
    , {"id": 'B12', name: 'Woodblock', file: 'woodblock.wav'}
    , {"id": 'S1', name: 'Eins', file: 'eins.wav'}
    , {"id": 'S2', name: 'Zwei', file: 'zwei.wav'}
    , {"id": 'S3', name: 'Drei', file: 'drei.wav'}
    , {"id": 'S4', name: 'Vier', file: 'vier.wav'}
    , {"id": 'U1', name: 'Und', file: 'und.wav'}
]

enum CountWaveIds {
    ONE = 'S1',
    TWO = 'S2',
    THREE = 'S3',
    FOUR = 'S4',
    AND = 'U1'
}

export const countSampleDefinitions: SampleDefinition[] = [
      {"id": CountWaveIds.ONE, name: 'Eins', file: 'eins.wav'}
    , {"id": CountWaveIds.TWO, name: 'Zwei', file: 'zwei.wav'}
    , {"id": CountWaveIds.THREE, name: 'Drei', file: 'drei.wav'}
    , {"id": CountWaveIds.FOUR, name: 'Vier', file: 'vier.wav'}
    , {"id": CountWaveIds.AND, name: 'Und', file: 'und.wav'}
]


export class BeatSetting {
    nr: number;
    selectedSample?: SampleDefinition = {"id": 'B1', name: 'Cowbell', file: 'cowbell.wav'};
    beats: (string | null)[];

     constructor(nr: number, beats: (string | null)[], sampleId: string) {
        this.nr = nr;
        this.selectedSample = beatSampleDefinitions.find(definition => definition.id === sampleId);
        this.beats = beats;
    }
}

export const defaultBeatSettings = [
    new BeatSetting(0,['C1', null, null, null, null, null, null, null], 'B11')
    , new BeatSetting(0,[null, null, 'C2', null, 'C2', null, 'C2', null], 'B8')
    , new BeatSetting(0,[null, 'C3', null, 'C3', null, 'C3', null, 'C3'], 'B12')
]


export class BeatSequence {
    sampleId: string;
    player;
    sequence;

    constructor(events: (string | null)[], sampleId: string) {

        this.sampleId = sampleId;
        this.player = new Tone.Player();
        //this.player.debug = true
        this.sequence = new Tone.Sequence(undefined, events, '8n');

    }

    start(buffers: ToneAudioBuffers) {
        this.player.buffer = buffers!.get(this.sampleId);
        this.player.toDestination();
        this.sequence.callback = ((time) => {

            this.player.start(time)
        });
        this.sequence.start();
    }

    mute(mute: boolean) {
        console.log("Mute beat seq " + mute);
        this.player.mute = mute
    }

    setEvents(beats: (string | null)[]) {
        this.sequence.events = beats;
    }

    setBuffer(buffer: Tone.ToneAudioBuffer) {
        this.player.buffer = buffer;
    }
}

/**
 * Sequence that count a bar from 1 to 4 by playing wav files
 * 1 and 2 and 3 and 4 8 (in german)
 */
export class CountSequence {
    note2WaveIdMap :  { [key: string]: string };
    players: { [key: string]: Tone.Player};
    sequence;

    constructor() {
        // map notes to wave ids,
        this.note2WaveIdMap =  {
            C1: CountWaveIds.ONE, C2: CountWaveIds.AND, C3: CountWaveIds.TWO, C4: CountWaveIds.AND
            , C5: CountWaveIds.THREE, C6: CountWaveIds.AND, C7: CountWaveIds.FOUR,C8: CountWaveIds.AND
        }
        this.players = {};
        // create sequence of events from note map
        const events = Object.keys(this.note2WaveIdMap);
        this.sequence = new Tone.Sequence(undefined, events, '8n');
    }
    start(buffers: ToneAudioBuffers) {

        // create array of wav players index by note
        for (const note in this.note2WaveIdMap) {
            const sampleId = this.note2WaveIdMap[note];
            this.players[note] = new Tone.Player();
            this.players[note].buffer =  buffers!.get(sampleId);
            this.players[note].toDestination();
            this.players[note].volume.value = -18;
        }
        // play the wav player that is mapped to the note of the sequence
        this.sequence.callback = ((time, note) => {
            this.players[note].start(time)
        });
        this.sequence.start();
    }

    mute(mute: boolean) {
        console.log("Mute count seq " + mute);
        Object.entries( this.players)
            .forEach(([, value]) => value.mute = mute);

    }

}


export class RhythmPlayer {

    countSequence: CountSequence = new CountSequence();
    beatSequences: BeatSequence[] = [];
    beatBuffers: ToneAudioBuffers | null = null;
    countBuffers: ToneAudioBuffers | null = null;
    timeSequence: (Tone.Sequence| null) = null;

    constructor(beatSettings: BeatSetting[]) {

        this.beatSequences = beatSettings.map((setting,)  =>
            new BeatSequence(setting.beats, setting.selectedSample?.id!));
        this.countSequence.mute(true); // mute counting initial
    }

    mute(mute: boolean) {
        this.beatSequences.forEach((sequence) => {
            sequence.mute(mute)
        } )
        //this.countSequence.mute(mute);
    }
    changeSettings(beatSettings: BeatSetting[]) {

        beatSettings.forEach((setting, i) => {
            this.beatSequences[i].setEvents (setting.beats);
            const bufferID = setting.selectedSample?.id
            if (this.beatBuffers && bufferID) {
                this.beatSequences[i].setBuffer(this.beatBuffers.get(bufferID))
            }
        } )
    }

    /**
     * Load wav sample's and start time sequence
     * @param timeCallback
     */
    load(timeCallback: ToneEventCallback<number>) {
        this.beatBuffers = this.loadAudioBuffers(beatSampleDefinitions);
        this.countBuffers = this.loadCountBuffers(countSampleDefinitions);
        this.timeSequence = new Tone.Sequence(timeCallback
            , [0, 1, 2, 3, 4, 5, 6, 7], '8n');
        this.timeSequence.start();
    }

    private loadAudioBuffers(definitions: SampleDefinition[]) : Tone.ToneAudioBuffers {
        const bufferUrls: { [key: string]: string } = {};
        definitions.forEach((item) => {
            bufferUrls[item.id] = item.file
        })

        const buffers = new ToneAudioBuffers({
            urls: bufferUrls,
            onload: () => {

                this.beatSequences.forEach((beatSeq) => {
                    if (this.beatBuffers) {
                        beatSeq.start(buffers);
                    }
                })
            },
            baseUrl: import.meta.env.BASE_URL + '/samples/'
        })
        return buffers;
    }

    private loadCountBuffers(definitions: SampleDefinition[]) : Tone.ToneAudioBuffers {
        const bufferUrls: { [key: string]: string } = {};
        definitions.forEach((item) => {
            bufferUrls[item.id] = item.file
        })

        const buffers =  new ToneAudioBuffers({
            urls: bufferUrls,
            onload: () => {

                this.countSequence.start(buffers);
                this.countSequence.mute(true);

            },
            baseUrl: import.meta.env.BASE_URL + '/samples/'
        });
        return buffers;
    }

    dispose() {

        this.beatSequences.forEach((beatSeq) => {
            beatSeq.player.disconnect();
            beatSeq.player.dispose();
            beatSeq.sequence.dispose()
            beatSeq.sequence.clear()
        });
        Tone.Transport.cancel ();
        this.timeSequence?.dispose()
    }
}

export class Kick {

    private audioCtx: AudioContext;
    private osc: OscillatorNode;
    private gain: GainNode ;

    constructor(audioCtx: AudioContext) {
        this.audioCtx = audioCtx;
        this.osc = this.audioCtx.createOscillator();
        this.gain = this.audioCtx.createGain();
    }

    setup() {
        this.osc = this.audioCtx.createOscillator();
        this.gain = this.audioCtx.createGain();
        this.osc.connect(this.gain);
        this.gain.connect(this.audioCtx.destination)
    }

    trigger (time: number) {
        this.setup();

        this.osc.frequency.setValueAtTime(150, time);
        this.gain.gain.setValueAtTime(1, time);

        this.osc.frequency.exponentialRampToValueAtTime(0.01, time + 0.5);
        this.gain.gain.exponentialRampToValueAtTime(0.01, time + 0.5);

        this.osc.start(time);

        this.osc.stop(time + 0.5);
    };
}



export class HiHat {

    private audioCtx: AudioContext;
    private buffer: AudioBuffer;
    private source: AudioBufferSourceNode;

    constructor(audioCtx: AudioContext, buffer: AudioBuffer) {
        this.audioCtx = audioCtx;
        this.buffer = buffer
        this.source = this.audioCtx.createBufferSource();

    }
    setup() {
        this.source = this.audioCtx.createBufferSource();
        this.source.buffer = this.buffer;
        this.source.connect(this.audioCtx.destination);
    }
    trigger (time: number) {
        this.setup();
        this.source.start(time);
    }
}


export class Snare {

    private audioCtx: AudioContext;
    private noise: AudioBufferSourceNode;
    private noiseEnvelope: GainNode;
    private osc: OscillatorNode;
    private oscEnvelope: GainNode;

    constructor(audioCtx: AudioContext) {
        this.audioCtx = audioCtx;
        this.osc = this.audioCtx.createOscillator();
        this.noiseEnvelope = this.audioCtx.createGain();
        this.noise = this.audioCtx.createBufferSource();
        this.oscEnvelope = this.audioCtx.createGain();
    }

    noiseBuffer() {
        var bufferSize = this.audioCtx.sampleRate;
        var buffer = this.audioCtx.createBuffer(1, bufferSize, this.audioCtx.sampleRate);
        var output = buffer.getChannelData(0);

        for (var i = 0; i < bufferSize; i++) {
            output[i] = Math.random() * 2 - 1;
        }

        return buffer;
    };

    setup() {
        this.noise = this.audioCtx.createBufferSource();
        this.noise.buffer = this.noiseBuffer();
        var noiseFilter = this.audioCtx.createBiquadFilter();
        noiseFilter.type = 'highpass';
        noiseFilter.frequency.value = 1000;
        this.noise.connect(noiseFilter);
        this.noiseEnvelope = this.audioCtx.createGain();
        noiseFilter.connect(this.noiseEnvelope);

        this.noiseEnvelope.connect(this.audioCtx.destination);
        this.osc = this.audioCtx.createOscillator();
        this.osc.type = 'triangle';

        this.oscEnvelope = this.audioCtx.createGain();
        this.osc.connect(this.oscEnvelope);
        this.oscEnvelope.connect(this.audioCtx.destination);
    }

    trigger (time: number) {
        this.setup();

        this.noiseEnvelope.gain.setValueAtTime(1, time);
        this.noiseEnvelope.gain.exponentialRampToValueAtTime(0.01, time + 0.2);
        this.noise.start(time)

        this.osc.frequency.setValueAtTime(100, time);
        this.oscEnvelope.gain.setValueAtTime(0.7, time);
        this.oscEnvelope.gain.exponentialRampToValueAtTime(0.01, time + 0.1);
        this.osc.start(time)

        this.osc.stop(time + 0.2);
        this.noise.stop(time + 0.2);
    };
}
