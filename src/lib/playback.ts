
import {BeatPerChordLine, BeatPerChordParagraph, ChordLyricsPairWithBeat} from "./chordpro.ts";
import * as Tone from "tone";
import {BeatSetting, RhythmPlayer} from "./rhythm.ts";

export interface PlaybackBeatLineInfo {

    lineNumber: number;
    startBeat: number;
    chords: ChordLyricsPairWithBeat[];
}

export interface PlaybackInformation {

    currentLine: PlaybackBeatLineInfo | null;
    nextLine: PlaybackBeatLineInfo | null;
    currentChord: ChordLyricsPairWithBeat | null;
    songDurationSec: number;
    allLinesWithLyrics: PlaybackBeatLineInfo[];
    playTimeMs: number;
    beat: number;
    beatInSong: number;
    countInBars: number;


    calculateAllLinesWithLyric(songParagraphs: BeatPerChordParagraph[]): void;
    setBeatInSong(beatInSong: number): void;
    start(): void;
    stop(): void;
}

export class PlaybackInformationImpl implements PlaybackInformation {

    currentLine: PlaybackBeatLineInfo | null = null;
    nextLine: PlaybackBeatLineInfo | null = null;
    currentChord: ChordLyricsPairWithBeat | null = null;
    songDurationSec: number = 0;
    allLinesWithLyrics: PlaybackBeatLineInfo[] = [];
    playTimeMs: number = -1;
    beat: number = 0;
    beatInSong: number = 0;
    countInBars: number = 0


    calculateAllLinesWithLyric(songParagraphs: BeatPerChordParagraph[]) {

        const lines: BeatPerChordLine[] = [];
        songParagraphs.forEach(paragraph => {
            lines.push(...paragraph.lines);
        })

        this.allLinesWithLyrics = this.lineStartBeatWithLyrics(lines) ;
    }


    lineStartBeatWithLyrics(lines: BeatPerChordLine[]): PlaybackBeatLineInfo[] {

        return  lines
            .map(line => this.lineToBeatInfo(line))
            .filter(this.notEmpty);
    }

    notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
        return value !== null && value !== undefined;
    }

    lineToBeatInfo(line: BeatPerChordLine): PlaybackBeatLineInfo | null {

        if (line.lineNumber != null &&  line.chords.length > 0) {
            const firstItem = line.chords[0] as ChordLyricsPairWithBeat;
            const chords= line.chords
                .filter(item => item.beat >0 && item.chords);
            return { lineNumber: line.lineNumber, startBeat: firstItem.startBeatInSong, chords: chords };
        } {
            return null;
        }
    }

    setBeatInSong(beatInSong: number) {

        this.beatInSong = beatInSong;
        const firstLine =  this.allLinesWithLyrics.slice().reverse()
            .find ( line => (line.startBeat <= beatInSong && line.chords.length > 0))

        const nextLine =  this.allLinesWithLyrics
            .find ( line => (line.startBeat > beatInSong  && line.chords.length > 0))

        if(firstLine ) {
            this.currentLine = firstLine
            const chord = firstLine.chords.slice().reverse().find(chord => chord.startBeatInSong <= beatInSong);
            this.currentChord = chord ? chord : null;
        }
        if(nextLine ) {
            this.nextLine = nextLine
        }
    }

    start() {
    }

    stop() {

        this.currentLine = null;
        this.nextLine = null;
        this.playTimeMs = 0;
    }
}

export interface PlaybackControl {

    playbackInfo: PlaybackInformation;

    start(): void;

    stop(): void;

    setSongParagraphs(songParagraphs: BeatPerChordParagraph[]): void;

}

/**
 * Playback controlled by an intervall with a  fixed time delay
 */
export class PlaybackByMp3 implements PlaybackControl {

    playbackInfo: PlaybackInformation;
    private startTimeCtx = 0;
    private audioCtx = new AudioContext();
    private mp3Source: AudioBufferSourceNode | null = null;
    private _audioBuffer: AudioBuffer | null;
    private intervalID: number | null  = null;

    constructor(audioBuffer: AudioBuffer | null, info: PlaybackInformation) {
        this._audioBuffer = audioBuffer;
        this.playbackInfo = info;
        if(this._audioBuffer != null) {
            this.playbackInfo.songDurationSec = Math.round(this._audioBuffer.duration)
        }
    }


    set audioBuffer(value: AudioBuffer | null) {
        this._audioBuffer = value;
        if(this._audioBuffer != null) {
            this.playbackInfo.songDurationSec = Math.round(this._audioBuffer.duration)
        } else {
            this.playbackInfo.songDurationSec = 0;
        }
    }

    setSongParagraphs(songParagraphs: BeatPerChordParagraph[]) {

        if(songParagraphs) {
            this.playbackInfo.calculateAllLinesWithLyric(songParagraphs);
        }
    }

    increment() {
        this.playbackInfo.playTimeMs = ( Math.round((this.audioCtx.currentTime - this.startTimeCtx) * 1000));
    }


    start() {

        this.audioCtx.resume();
        const bufferSource: AudioBufferSourceNode =  new AudioBufferSourceNode(this.audioCtx, {buffer: this._audioBuffer});
        bufferSource.connect(this.audioCtx.destination);
        this.mp3Source = bufferSource;
        if(bufferSource?.buffer?.duration) {
            this.playbackInfo.songDurationSec = bufferSource?.buffer?.duration;
        }
        this.startTimeCtx = this.audioCtx.currentTime + 0.01;
        if (this.mp3Source) {
            this.mp3Source.start(this.startTimeCtx)
            this.intervalID = window.setInterval(() => {
                this.increment()
            }, 500);
        }
    }


    stop() {

        this.playbackInfo.stop()
        if(this.mp3Source ) {
            this.mp3Source.stop()
            this.mp3Source = null;
        }
        if (this.intervalID) {
            clearInterval(this.intervalID);
            this.intervalID = null;
        }
    }

}

/**
 * Playback controlled by an intervall with a  fixed time delay
 */
export class PlaybackRhythm implements PlaybackControl {

    playbackInfo: PlaybackInformation;
    private beatSettings: BeatSetting[]
    private rhythmPlayer: RhythmPlayer;
    private lastIncrementTime = 0;
    private bpm = 60;
    private muted = false;
    private startBeat = 1;

    constructor(info: PlaybackInformation, beatSettings: BeatSetting[]) {

        this.beatSettings = beatSettings;
        this.rhythmPlayer = new RhythmPlayer(beatSettings)
        this.playbackInfo = info;
    }

    start(): void {

        this.lastIncrementTime = Date.now();
        this.rhythmPlayer.dispose()
        this.rhythmPlayer = new RhythmPlayer(this.beatSettings)
        this.rhythmPlayer.load((_time, note) => {

            const playTimeMs = this.playbackInfo.playTimeMs + (Date.now() - this.lastIncrementTime);
            this.playbackInfo.playTimeMs = playTimeMs;
            this.lastIncrementTime = Date.now();
            this.playbackInfo.beat = note;
            this.playbackInfo.setBeatInSong( this.playbackInfo.beatInSong += 0.5); // one note is a half beat
/*            console.log("_time " + _time ) */
            console.log("note " + note )
            console.log("beatInSong " + this.playbackInfo.beatInSong )
        });

        this.playbackInfo.beatInSong = this.startBeat;
        Tone.Transport.loop = true;
        Tone.Transport.loopEnd = '1m';
        Tone.Transport.bpm.value = this.bpm;
        this.rhythmPlayer.mute(this.muted)
        Tone.start()
        Tone.Transport.start();
    }

    stop(): void {
        Tone.Transport.stop();
        this.lastIncrementTime = Date.now();
        this.playbackInfo.beatInSong = this.startBeat -1;
        this.playbackInfo.stop()
        this.rhythmPlayer.dispose()
        Tone.Transport.cancel();
    }

    setSongParagraphs(songParagraphs: BeatPerChordParagraph[]) {

        if(songParagraphs) {
            this.playbackInfo.calculateAllLinesWithLyric(songParagraphs);
        }

    }

    setCountInBeats(countInBeats: number) {
        this.startBeat = 1 - countInBeats - 0.5;
    }

    changedBpm(newBpm: number) {
        this.bpm = newBpm;
        Tone.Transport.bpm.value = newBpm;
    }

    changeSettings(beatSettings: BeatSetting[]) {
        this.beatSettings = beatSettings;
        this.rhythmPlayer.changeSettings(beatSettings);
    }

    mute(mute: boolean) {
        this.muted = mute;
        this.rhythmPlayer.mute(mute);
    }
}