import ChordSheetJS, {ChordLyricsPair, Comment, Line, Song, Tag, Ternary} from 'chordsheetjs';

export interface ParsedParagraph {
    lines: Line[]
    startTag: Tag | null
}

export interface ConvertedParagraph {
    lines: BeatPerChordLine[]
    startTag: Tag | null
}

export class SongWithBeatPerChordParser {

    importSongFrom(chordProSong: Song): BeatPerChordParagraph[] {

        let parsedParagraphs: ParsedParagraph[] = [];
        let linesOfCurrentParagraph: Line[] = [];
        let lastStartTag: Tag | null = null;

        chordProSong.lines.forEach(line => {

            const currentStartOfTag = this.findStartOfTagInLine(line);
            if (this.hasChordOrLyricItem(line) || this.hasBeatsPerChordTag(line)) {
                linesOfCurrentParagraph.push(line);
            } else if (currentStartOfTag) {
                if (linesOfCurrentParagraph.length > 0) {
                    parsedParagraphs.push({ lines: linesOfCurrentParagraph, startTag: lastStartTag});
                    linesOfCurrentParagraph = [];
                }
                lastStartTag = currentStartOfTag;
            } else if (this.hasEndOfTag(line)) {
                if (linesOfCurrentParagraph.length > 0) {
                    parsedParagraphs.push({ lines: linesOfCurrentParagraph, startTag: lastStartTag});
                    linesOfCurrentParagraph = [];
                }
                lastStartTag = null;
            }
        })
        if (linesOfCurrentParagraph.length > 0) {
            parsedParagraphs.push({ lines: linesOfCurrentParagraph, startTag: lastStartTag});
        }
        console.log("addTimesToParagraphs")
        const convertedParagraphs: ConvertedParagraph[] = parsedParagraphs
            .map( paragraph => this.convertLinesToParagraphs(paragraph))

        let currentBeatInSong = 1;
        const chordParagraphs = convertedParagraphs.map(paragraph => {
            const chordParagraph: BeatPerChordParagraph = new BeatPerChordParagraph(paragraph.lines, paragraph.startTag);
            currentBeatInSong = this.calculateAbsoluteBeats(currentBeatInSong, chordParagraph);
            return chordParagraph;
        })
        console.log("finished")

        return chordParagraphs;
    }

    findStartOfTagInLine(line: Line) : Tag | null {

        const firstTagInLine : Tag | null  = (line.items.length > 0 && line.items[0] instanceof Tag) ? line.items[0] : null;
        return (this.isStartOfLineTag(firstTagInLine) ? firstTagInLine : null);
    }

    isStartOfLineTag(tagToCheck: Tag | null): boolean {
        return !!((tagToCheck) && (tagToCheck.name) && (tagToCheck?.name.toLowerCase().startsWith('start_of')
            || tagToCheck.name.toLowerCase().startsWith('so')));
    }

    hasEndOfTag(line: Line) : boolean {

        return line.items.length > 0 && line.items[0] instanceof Tag && !!line.items[0].name
            && (line.items[0].name.toLowerCase().startsWith('end_of') || line.items[0].name.toLowerCase().startsWith('eo'))
    }

    hasChordOrLyricItem(line: Line) : boolean {

        const notEmptyItems = line.items
            .filter(item => item instanceof ChordLyricsPair && isChordLyricsPairNotEmpty(item))

        return notEmptyItems.length > 0;

    }

    hasBeatsPerChordTag(line: Line) : boolean {

        return line.items.length > 0 && line.items[0] instanceof Tag && !!line.items[0].name
            && line.items[0].name.toLowerCase().startsWith('x_beats_per_chord')
    }

    convertLinesToParagraphs(parsedParagraph:ParsedParagraph): ConvertedParagraph {

        const convertedlines: BeatPerChordLine[] = []
        parsedParagraph.lines.forEach((chordLine, indexChordsline) => {
                if (this.hasChordOrLyricItem(chordLine)) {

                    const beatsPerChordLine = (indexChordsline + 1 < parsedParagraph.lines.length
                         && this.hasBeatsPerChordTag( parsedParagraph.lines[indexChordsline+1])) ? parsedParagraph.lines[indexChordsline+1]: null;
                    const convertedItems = this.convertLineToItemsWithBeats(chordLine, beatsPerChordLine)
                    const chordLineWithBeat: BeatPerChordLine = new BeatPerChordLine(convertedItems, chordLine.lineNumber);
                    convertedlines.push(chordLineWithBeat);
                }
            }
        )

        return {lines: convertedlines, startTag: parsedParagraph.startTag};
    }

    calculateAbsoluteBeats(startBeat: number, chordParagraph: BeatPerChordParagraph ) {

        chordParagraph.startBeat = startBeat;
        let currentBeat = startBeat;
        chordParagraph.lines.forEach(line => {
            line.chords.forEach(item => {
                item.startBeatInSong = currentBeat;
                currentBeat = currentBeat + item.beat;
            })
        })
        chordParagraph.endBeat = currentBeat;
        return currentBeat;
    }

    convertLineToItemsWithBeats(chordLine: Line, beatsPerChordLine: Line | null) : ChordLyricsPairWithBeat[] {

        const chordTimes = (beatsPerChordLine) ? this.beatsPerChordInLine(beatsPerChordLine) : [];
        const convertedItems: ChordLyricsPairWithBeat[] = []
        for (let i = 0; i < chordLine.items.length; i++) {
            let curItem= chordLine.items[i];
            let beatString = '';
            if( curItem instanceof ChordLyricsPair && curItem.chords.length > 0)  {
                const chordTime  = chordTimes.shift();
                if(chordTime) {
                    beatString = chordTime;
                }
            }
            convertedItems.push(new ChordLyricsPairWithBeat(chordLine.items[i] as ChordLyricsPair, beatString));
        }
        return convertedItems;
    }

    beatsPerChordInLine(timesLine: Line): string[] {

        const timesItem = timesLine.items[0] as Tag;
        return timesItem.value.split(' ');
    }
}


export interface SongWithBeatPerChordI {
    paragraphs: BeatPerChordParagraph[];
    title: string;
    transposeKey: string | null;
    beatsInBar: number;
}

export class SongWithBeatPerChord implements SongWithBeatPerChordI {

    private _chordProSong: Song
    paragraphs: BeatPerChordParagraph[] = []
    title: string = ""
    transposeKey: string | null = null
    beatsInBar: number = 4

    constructor(songTextCordPro: string, transposeToEasyChords: boolean) {

        const chordProParser = new ChordSheetJS.ChordProParser();
        this._chordProSong = chordProParser.parse(songTextCordPro);

        const x_transpose = this._chordProSong.getMetadata("x_transpose")
        if (x_transpose && transposeToEasyChords) {
            this.transposeKey = x_transpose as string;
            this._chordProSong = transposeSongTo(this._chordProSong, x_transpose as string)
        }
        const parser = new SongWithBeatPerChordParser();
        this.paragraphs = parser.importSongFrom(this._chordProSong);
        this.title = this._chordProSong.title;
    }


    transposeSongTo (key: string) {

        if (this._chordProSong) {
            if (this._chordProSong.metadata.key) {
                this._chordProSong = this._chordProSong.changeKey(key)
            } else {
                const firstChord = firstChordInSong(this._chordProSong.lines)
                this._chordProSong = this._chordProSong.setKey(firstChord).changeKey(key)
            }
            this.paragraphs = new SongWithBeatPerChordParser().importSongFrom(this._chordProSong);
        }
    }


    allChordsInSong(): Set<string> {

        const chords = new Set<string>();
        if (this._chordProSong) {
            const lines: Line[] = this._chordProSong.lines
            lines.forEach((line) => {

                //console.log( `${line.lineNumber}:  Type: ${line.type}:`);
                //console.log(isCommentLine(line))
                line.items.forEach(item => {
                    if (item instanceof ChordLyricsPair ) {
                        //console.log( `Chord: ${item.chords} ${item.lyrics}  `);
                        if(item.chords.trim()) {
                            chords.add(item.chords)
                        }
                    } else if (item instanceof Comment) {
                        //console.log( `Comment: ${item.content}:  `);
                    } else if (item instanceof Tag) {
                        //console.log( `Tag: ${item.name} Value: ${item.value}`);
                    } else if (item instanceof Ternary) {
                        //console.log( `Ternary: ${item.line}`);
                    }
                })
            });
        }
        return chords;
    }

    get chordProSong(): Song {
        return this._chordProSong;
    }

    countInBeats(countInBars: number): number {
        return countInBars * this.beatsInBar;
    }
}


export class BeatPerChordParagraph {

    lines: BeatPerChordLine[] = [];
    paragraphType: string = ""
    paragraphName: string = ""
    startBeat : number = -1;
    endBeat : number = 0;


    constructor(lines: BeatPerChordLine[], startTag: Tag | null) {
        this.lines = lines;
        this.paragraphType = this.startOfTagType(startTag);
        this.paragraphName = this.tagValue(startTag);
    }

    startOfTagType(startTag: Tag | null) : string {

        const name =  startTag ? startTag.name : null;
        return !!name ? name.replace('start_of_','').replace('so', '') : "";
    }

    tagValue(startTag: Tag | null) : string {

        return  startTag ? startTag.value : "";
    }


    name(): string {
        return (!!this.paragraphName) ? this.paragraphName : this.paragraphType
    }
}

export class BeatPerChordLine {
    chords: ChordLyricsPairWithBeat[] = [];
    lineNumber: number | null;

    constructor(chords: ChordLyricsPairWithBeat[], lineNumber: number | null) {
        this.chords = chords;
        this.lineNumber = lineNumber;
    }
}

export class ChordLyricsPairWithBeat extends ChordLyricsPair {
    beat : number;
    startBeatInSong : number;
    constructor(pairToCopy: ChordLyricsPair, timeString : string) {
        super();
        this.chords = pairToCopy.chords;
        this.lyrics = pairToCopy.lyrics;
        this.beat = Number(timeString);
        this.startBeatInSong = -1;
        if(isNaN(this.beat) || timeString.length === 0) {
            this.beat = 0;
        }
    }

    hasBeatInformation() : boolean {

        return this.beat > 0;
    }
}

export interface ChordDisplaySettings {
    showPlayTimeInLine: boolean
    showBeatInChord: boolean
}

export function allChordsInSong(lines: Line[]): Set<string> {

    const chords = new Set<string>();
    lines.forEach((line) => {

        //console.log( `${line.lineNumber}:  Type: ${line.type}:`);
        //console.log(isCommentLine(line))
        line.items.forEach(item => {
            if (item instanceof ChordLyricsPair ) {
                //console.log( `Chord: ${item.chords} ${item.lyrics}  `);
                if(item.chords.trim()) {
                    chords.add(item.chords)
                }
            } else if (item instanceof Comment) {
                //console.log( `Comment: ${item.content}:  `);
            } else if (item instanceof Tag) {
                //console.log( `Tag: ${item.name} Value: ${item.value}`);
            } else if (item instanceof Ternary) {
                //console.log( `Ternary: ${item.line}`);
            }
        })
    });

    return chords;
}

export function firstChordInSong(lines: Line[]): string {

    const firstChordInSong = lines.map(line => {
        const firstChordInLine = line.items.find(item =>  item instanceof ChordLyricsPair && item.chords.trim().length > 0)
        return firstChordInLine as ChordLyricsPair
    }).find(chord  =>  chord != null)

    return firstChordInSong ? firstChordInSong.chords : 'C';
}

export function removeEmptyChords(lines: Line[]): void {

    // remove empty lyrics
    lines.forEach((line) => {

        line.items = line.items.filter(item => !(item instanceof ChordLyricsPair )
            || item.chords.trim().length > 0 || (item?.lyrics?.trim()?.length && item?.lyrics?.trim()?.length >0))
    });

    lines.forEach((line) => {

        line.items.forEach(item => {
            if (item instanceof ChordLyricsPair) {
                item.chords = item.chords.trim();
            }
        } )
    });

}

export function songDurationInSeconds(song: Song): number {
    const duration = song.metadata.duration;
    return (duration) ? parseDuration2Seconds(duration) : 0;
}

export function parseDuration2Seconds(durationString: string) {
    let parts = durationString.split(":");

    let parsedSeconds = 0;
    if (parts.length === 0) {
        parsedSeconds =  Number(parts[0]);
    } else if (parts.length === 1) {
        parsedSeconds =  Number(parts[0]);
    } else if (parts.length === 2) {
        parsedSeconds = Number(parts[0]) * 60 + Number(parts[1])
    } else {
        parsedSeconds = Number(parts[0]) * 60 * 60 + Number(parts[1]) * 60  + Number(parts[2])
    }
    return (parsedSeconds) ?  parsedSeconds : 0; // check NaN
}

export function seconds2TimeString(timeInSeconds : number | undefined) {

    const timeToDisplay = (timeInSeconds && timeInSeconds >= 0) ? timeInSeconds : 0;
    const time = Math.round(timeToDisplay);
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;

    return `${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`
}

export function transposeSongTo (song: Song, key: string) {

    let tranposedSong = song;
    if (song.metadata.key) {
        tranposedSong = song.changeKey(key)
    } else {
        const firstChord = firstChordInSong(song.lines)
        tranposedSong = song.setKey(firstChord).changeKey(key)
    }
    return tranposedSong;
}


function isChordLyricsPairNotEmpty(pair:ChordLyricsPair) {
    return  (!!pair.lyrics  && pair.lyrics?.trim().length > 0) || pair.chords.trim().length > 0
}

