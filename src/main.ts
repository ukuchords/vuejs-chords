import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { Quasar, AppFullscreen } from 'quasar'
import quasarIconSet from 'quasar/icon-set/material-icons-outlined'
import router from './router'

// Import icon libraries
import '@quasar/extras/roboto-font-latin-ext/roboto-font-latin-ext.css'
import '@quasar/extras/material-icons/material-icons.css'
// ..required because of selected iconSet:
import '@quasar/extras/material-icons-outlined/material-icons-outlined.css'
import 'quasar/src/css/index.sass'


const myApp = createApp(App)
myApp.use(Quasar, {
    plugins: {AppFullscreen},
    iconSet: quasarIconSet,
})

myApp.use(router)
myApp.mount('#app')
