# vuejs-chords

Ukulele Play along Chords is an application to display a song with its chords in such a way that the 
whole width of the screen is utilized.

It has a playback function to play a rythm. It shows the current position  and the current chord while playing.

## Song Format 

The [ChordPro](https://www.chordpro.org/chordpro/chordpro-introduction/) format is used for the songs.
For the playback a custom extention is added to the format.


