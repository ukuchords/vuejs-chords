import { expect, test } from 'vitest'
import {
    firstChordInSong,
    parseDuration2Seconds, removeEmptyChords, SongWithBeatPerChord, SongWithBeatPerChordParser
} from "../src/lib/chordpro";
import ChordSheetJS, {ChordLyricsPair} from "chordsheetjs";
import { readFile } from "fs/promises"
import {PlaybackInformationImpl} from "../src/lib/playback";

test('swing low', () => {

    let songText = `# A simple ChordPro song.

{title: Swing Low Sweet Chariot}

{start_of_chorus}
[D]Swing [D]low, sweet [G]chari[D]ot,
{x_beats_per_chord: 2 4 2 4}
Comin’ for to carry me [A7]home.
{x_beats_per_chord: 4}
Swing [D7]low, sweet [G]chari[D]ot,
{x_beats_per_chord: 2 4 4}
Comin’ for to [A7]carry me [D]home.
{x_beats_per_chord: 4 4}
{end_of_chorus}

{start_of_verse}
 Comin’ for to carry me [A7]home.
 {x_beats_per_chord: 4}
A [D]band of angels [G]comin’ after [D]me,
{x_beats_per_chord: 4 2 4}
Comin’ for to [A7]carry me [D]home.
{x_beats_per_chord: 4 6}
{end_of_verse}
        `
    const songWithBeat = new SongWithBeatPerChord(songText, false);
    expect(songWithBeat.paragraphs.length).toBe(2)
    expect(songWithBeat.paragraphs[0].name()).toBe("chorus");
    expect(songWithBeat.paragraphs[1].name()).toBe("verse");
    expect(songWithBeat.paragraphs[0].lines.length).toBe(4);
    expect(songWithBeat.paragraphs[1].lines.length).toBe(3);
    expect(songWithBeat.paragraphs[0].lines[0].chords.length).toBe(5);
    expect(songWithBeat.paragraphs[0].lines[0].chords[0].beat).toBe(2);
    expect(songWithBeat.paragraphs[0].lines[0].chords[0].lyrics).toBe("Swing ");
    expect(songWithBeat.paragraphs[0].lines[0].chords[0].chords).toBe("D");
    expect(songWithBeat.paragraphs[0].lines[0].chords[1].beat).toBe(4);
})

test('westerland', () => {
    let songText = `{title: Westerland}
{artist: Die Ärzte}
{comment: Intro}
C - G - Am - F
C - G - Am - F
[C]Ah   ah [G]  ah[Am][F]
[C]Ah   ah [G] ah    [Am] Oho[F]
{start_of_verse}
[C]Jeden Tag sitz’ ich am Wannsee
Und ich hoer [G]den Wellen zu
[C]Ich lieg hier auf meinem Handtuch
Doch ich finde[G]keine Ruh
[F]Diese eine Liebe w[Am]ird nie zu Ende gehen.
[F]Wa[G]nn werd ich sie wieder sehen.
{end_of_verse}
{start_of_verse: Verse 1}
[C]Manchmal schließe ich die Augen,
G\tStell mir vor ich sitz am Meer.
[C]Dann denk ich an diese Insel  \t \t \t                             G
Und mein herz das wird so schwer
[F]Diese eine Liebe w[Am]ird nie zu Ende gehen
[F]Wann werd [G]ich sie wieder sehen
{end_of_verse}
{start_of_chorus}
[C]Oh ich[G] hab sol[Am]che Sehn[F]sucht
[C]Ich verliere d[G]en Versta[Am]nd[F]
[C]Ich will[G] wieder an die [Am]Nords[F]ee ohoho
[Dm]Ich wi[F]ll zurück nac[G]h West[C]erland
{end_of_chorus}`

    const songWithBeat = new SongWithBeatPerChord(songText, false);
    expect(songWithBeat.paragraphs.length).toBe(4)
    expect(songWithBeat.paragraphs[0].name()).toBe("");
    expect(songWithBeat.paragraphs[1].name()).toBe("verse");
    expect(songWithBeat.paragraphs[2].name()).toBe("Verse 1");
    expect(songWithBeat.paragraphs[3].name()).toBe("chorus");
})

test('transpose to C', () => {

    let songText =
        `[D]Swing [D]low, sweet [G]chari[D]ot,
Comin’ for to carry me [A7]home.`

    const parser = new ChordSheetJS.ChordProParser();
    const song = parser.parse(songText);
    const transposed = song.setKey('D').changeKey('C')

    expect(song.metadata.key).toBeUndefined()
    expect(transposed.lines.length).toBe(3)
    expect(transposed.metadata.key).toBe('C')
    const item_1_1 =transposed.lines[1].items[1] as ChordLyricsPair
    expect(item_1_1.chords).toBe('C')
})

test('parseDuration2Seconds', () => {

    const durationFromSeconds = parseDuration2Seconds('268')
    expect(durationFromSeconds).toBe(268)
    const durationFromMinutes = parseDuration2Seconds('4:28')
    expect(durationFromMinutes).toBe(268)
    const durationFromHours = parseDuration2Seconds('0:04:28')
    expect(durationFromHours).toBe(268)
    const durationFromHours2 = parseDuration2Seconds('01:04:28')
    expect(durationFromHours2).toBe(3868)
    const wrongDuration = parseDuration2Seconds('abc')
    expect(wrongDuration).toBe(0)

})

test('first chord in song', () => {

    const parser = new ChordSheetJS.ChordProParser();
    const songInF = parser.parse(`[F]Diese eine Liebe w[Am]ird nie zu Ende gehen.
[F]Wa[G]nn werd ich sie wieder sehen`);
    expect(firstChordInSong(songInF.lines)).toBe('F')

    const songInG = parser.parse(`Diese eine Liebe wird nie zu Ende gehen.
Wa[G]nn werd ich sie wieder sehen`);
    expect(firstChordInSong(songInG.lines)).toBe('G')

    const songWithoutChord = parser.parse(`Diese eine Liebe wird nie zu Ende gehen.`);
    expect(firstChordInSong(songWithoutChord.lines)).toBe('C')
})

test('whatsup', () => {
    let songText =
`{title: Whats Up}
{artist:4 Non Blondes}
{key: G}
{duration: 4:52}
{start_of_verse: Intro}
[G] [Am] [C] [G]
{x_time_of_chord: 0 4 8 12}
[G] [Am] [C] [G]
{x_time_of_chord: 16 20 24 28}
{end_of_verse}
{start_of_verse}
[G]Twenty-five years and my life is still
{x_time_of_chord: 30}
[Am]Trying to get up that great big hill
{x_time_of_chord: 34}
Of [C]hope for a desti[G]nation
{x_time_of_chord: 40 44}
I [G]realized quickly when I knew I should
{x_time_of_chord: 44 }
That the [Am]world was made up of this brotherhood
{x_time_of_chord: 48 }
Of [C]man, for whatever that [G]means
{x_time_of_chord: 52 55}
{end_of_verse}

{comment: Pre-Chorus}
And so I [G]cry sometimes when I'm lying in bed
{x_time_of_chord: 58}
Just to [Am]get it all out, what's in my head
{x_time_of_chord: 62}
and I [C]- I am feeling a little pe[G]culiar
{x_time_of_chord: 66 70}
And so I [G]wake in the morning and I step outside
{x_time_of_chord: 74 }
and I [Am]take a deep breath and I get real high and
{x_time_of_chord: 76 }
I [C]scream from the top of my lungs "What's going [G]on?"
{x_time_of_chord: 80 84}`

    const songWithBeat = new SongWithBeatPerChord(songText, false);
    expect(songWithBeat.paragraphs.length).toBe(3)
    expect(songWithBeat.paragraphs[0].name()).toBe("Intro");
    expect(songWithBeat.paragraphs[1].name()).toBe("verse");
    expect(songWithBeat.paragraphs[2].name()).toBe("")
})

test('read from file Dacing_In_The_Dark.cho', async  () => {

    const songFile = await readFile(`${__dirname}/../public/songs/Dacing_In_The_Dark.cho`, "utf8")
    const songWithBeat = new SongWithBeatPerChord(songFile, false);

    expect(songWithBeat.paragraphs.length).toBe(10)
    //expect((paragraphs[1].lines[3].items[1] as ChordLyricsPairWithBeat).beat).toBe(21)
    //expect((paragraphs[1].lines[3].items[0] as ChordLyricsPairWithBeat).beat).toBeNull()

})

test('read from file Wonderwall.cho', async  () => {

    const songFile = await readFile(`${__dirname}/../public/songs/Wonderwall.cho`, "utf8")
    const songWithBeat = new SongWithBeatPerChord(songFile, false);

    expect(songWithBeat.paragraphs.length).toBe(11)
    //expect((paragraphs[1].lines[3].items[1] as ChordLyricsPairWithBeat).beat).toBe(21)
    //expect((paragraphs[1].lines[3].items[0] as ChordLyricsPairWithBeat).beat).toBeNull()

})
test('removeEmptyChords', async  () => {
    let songText = `
        [G    ]B     [3-5-5-4-3-3]7-9-9-8-7-7
[C    ]Cm    [8-10-10-9-8-8]8-10-10-8-8-8


{comment: Intro}
[G][B][C][Cm]

{start_of_verse}
[                    ]When you were here be[G                             ]fore, couldn't look you in the [B]eyes
[                   ]You're just like an [C                        ]angel, your skin makes me [Cm]cry
[                ]You float like a [G                     ]feather in a beautiful [B]world
[            ]I wish I was [C                         ]special, you're so fucking [Cm]special
{end_of_verse}

{start_of_chorus}
[Cm](x3, very short)
[         ]But I'm a [G           ]creep, I'm a [B]weirdo
[                        ]What the hell am I doing [C              ]here? I don't be[Cm]long here
{end_of_chorus}

{start_of_verse}
[                  ]I don't care if it [G                     ]hurts, I wanna have con[B]trol
[                ]I want a perfect [C                     ]body, I want a perfect [Cm]soul
[             ]I want you to [G                   ]notice when I'm not a[B]round
[                 ]You're so fucking [C                    ]special, I wish I was [Cm]special
{end_of_verse}

{start_of_chorus}
{end_of_chorus} `

    const chordProParser = new ChordSheetJS.ChordProParser();
    const song = chordProParser.parse(songText)
    removeEmptyChords(song.lines)
    //expect(song.lines.length).toBe(30)
})

test('playback from file Wonderwall.cho', async  () => {

    const songFile = await readFile(`${__dirname}/../public/songs/Wonderwall.cho`, "utf8")
    const songWithBeat = new SongWithBeatPerChord(songFile, false);

    const playback = new PlaybackInformationImpl();
    playback.calculateAllLinesWithLyric(songWithBeat.paragraphs)

    expect(playback.allLinesWithLyrics.length).toBe(44)
    //expect((paragraphs[1].lines[3].items[1] as ChordLyricsPairWithBeat).beat).toBe(21)
    //expect((paragraphs[1].lines[3].items[0] as ChordLyricsPairWithBeat).beat).toBeNull()

})